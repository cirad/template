## 1 - REDIGER UN FICHIER README :

Le fichier README.md centralise tous les éléments du projet.
Conseils pour rédiger un README : [makeareadme.com](https://www.makeareadme.com/). Il vaut mieux un fichier README exhaustif plutot que raccourci. D'autres modes de documentation peuvent venir en appui tels que le Wiki ou le site web Gitlab Pages.

Elements à indiquer :

- personne contact
- rappel de la licence
- description de l'arborescence du projet : localisation de(s) documentation(s), du code source, fichiers exemples, positionnement du Wiki ou d'un site web
- informations des sous-sections suivantes

# Nom de projet
Choisir un nom explicit.

# Description

Indiquer ce que le projet permet de faire. Fournir le contexte et ajouter des liens vers des références bibliographiques si nécessaires. Lister les alternatives au projet ici (ex: interactions avec un autre dépôt).

# Installation

Considérer que le README peut être lu par des novices et donc lister toutes les étapes nécessaires pour installer le logiciel. 
Lister sous forme d'étapes spécifiques la procédure afin de lever les ambiguités. Ajouter une section 'prérequis' si des systèmes d'exploitation, environnements, ou librairies particulières sont nécessaires.

# Utilisation

Utiliser des exemples et montrer le résultat attendu si possible. Proposer un exemple simplifié et un exemple complexe, quitte à indiquer un lien spécifique dans ce seconde cas (ex: lien vers Dataverse). 

# Support
Indiquer quels sont les moyens d'obtenir de l'aide (issue, chat, email, etc ...).

# Roadmap
Si de nouvelles idées sont évoquées, elles peuvent être exposées dans le README.

# Contributions
Indiquer si le projet est ouvert aux contributions extérieures et quelles sont les critères pour les accépter.
Pour les personnes souhaitant apporter des modifications au projet, il peut être appreciable d'élaborer une documantation spécifique (ex: script à executer, variables d'environnements à initialiser, etc ...).
Il peut être indiqué les commandes pour exécuter des tests du code pour éviter que les modifications proposées affectent certaines fonctionnalités.

# Auteurs et remerciements
Indiquer les auteurs et contributeurs.


# 2 - RAPPELS POUR DEBUTER UN PROJET :

# Ajout de fichiers

- [ ] [Creation](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) ou [chargement](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) de fichiers
- [ ] [Ajout de fichiers en lignes de commandes](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) ou push d'un dépôt pré existant avec les commandes suivantes:

```
cd existing_repo
git remote add origin https://gitlab.cirad.fr/ecosols/test.git
git branch -M master
git push -uf origin master
```

# Collaboration avec l'équipe

- [ ] [Inviter des membres et collaborateurs](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Créer un nouveau merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Fermer une issues à partir de merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Activer les approbations de merge request](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [merge automatiquelors d'une pipeline réussie](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

# Test et déploiement

Utilisation de l'intégration continue dans GitLab.

- [ ] [Débuter avec GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyse des vulnérabilités du code avec Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Mise en place d'environements](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

